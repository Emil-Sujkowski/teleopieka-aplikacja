import { BrowserRouter, Routes, Route } from "react-router-dom";
import DatabaseBrowser from "./views/DatabaseBrowser";
import PatientProfile from "./views/PatientProfile";
import LoginForm from "./views/LoginForm";
import RegisterForm from "./views/RegisterForm";
import AdminPanel from "./views/AdminPanel";
import DoctorPanel from "./views/DoctorPanel";
import MapView from "./views/MapView";
import RequestsView from "./views/RequestsView";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<LoginForm />} />
        <Route path="register" element={<RegisterForm />} />
        <Route path="patients" element={<DatabaseBrowser />} />
        <Route path="patient/:id" element={<PatientProfile />} />
        <Route path="admin" element={<AdminPanel />} />
        <Route path="doctor" element={<DoctorPanel />} />
        <Route path="map" element={<MapView />} />
        <Route path="requests" element={<RequestsView />} />
      </Routes>
    </BrowserRouter>
  );
}
