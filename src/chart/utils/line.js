import * as d3 from "d3";

export const line = ({ xKey, yKey, x, y }) =>
  d3
    .line()
    .x((d) => x(d[xKey]))
    .y((d) => y(d[yKey]));
