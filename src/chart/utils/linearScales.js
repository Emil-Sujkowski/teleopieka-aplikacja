import * as d3 from "d3";

export const getScales = (data, dataReader, chartDimensions) => {
  const { width, height, marginLeft, marginRight, marginTop, marginBottom } = chartDimensions;
  const { domain, value } = dataReader;
  const x = d3
    .scaleLinear()
    .domain([d3.min(data, (d) => d[domain]), d3.max(data, (d) => d[domain])])
    .range([0, width - marginLeft - marginRight]);
  const y = d3
    .scaleLinear()
    .domain([d3.min(data, (d) => d[value]), d3.max(data, (d) => d[value])])
    .nice(10)
    .range([height - marginTop - marginBottom, 0]);

  return { x, y };
};
