const managers = {};

export function getZoomManager(key) {
  return managers[key];
}

export function createZoomManager(id) {
  if (managers[id]) return getZoomManager(id);

  managers[id] = {};
  let currentTransform = { x: 0, y: 0, k: 1 };
  const registeredActions = new Set();

  function applyZoom({ transform }) {
    currentTransform = transform;
    registeredActions.forEach((fn) => fn(transform));
  }

  function registerAction(transformFn) {
    if (typeof transformFn !== "function") return;

    registeredActions.add(transformFn);
  }

  function getCurrentTransform() {
    return currentTransform;
  }

  return Object.assign(managers[id], { applyZoom, registerAction, getCurrentTransform });
}
