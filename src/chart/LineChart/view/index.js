import { getAxes } from "./axis";
import { getLineChart } from "./chartArea";
import { getClipPath } from "./clipPath";
import { getMnimap } from "./minimap";
import { getThresholds } from "./threshold";

export function drawLineChart(selection, model) {
  const { xAxis, yAxis } = getAxes(model);
  const clipPath = getClipPath(model);
  const lineChart = getLineChart(model);
  const { thresholdTop, thresholdBottom } = getThresholds(model);
  const minimap = getMnimap(model);

  return selection
    .call(clipPath)
    .call(lineChart)
    .call(xAxis)
    .call(yAxis)
    .call(thresholdTop)
    .call(thresholdBottom)
    .call(minimap);
}
