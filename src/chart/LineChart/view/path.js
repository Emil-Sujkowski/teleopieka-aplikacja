import { getZoomManager } from "../../utils/zoom";
import { line } from "../../utils/line";

export const getPath = (config, eventsConfig = {}) => {
  const {
    color,
    strokeWidth,
    id,
    scales: { x, y },
  } = config;

  const { zoomable = true, pointerEvents = "auto" } = eventsConfig;

  const zoomManager = getZoomManager(id);

  const path = (selection) => {
    const localPath = selection
      .append("path")
      .attr("stroke", color)
      .attr("stroke-width", strokeWidth ?? 1)
      .attr("fill", "none")
      .attr("d", line({ xKey: "time", yKey: "value", x, y }))
      .attr("pointer-events", pointerEvents);

    const zoomAction = (transform) => {
      const newX = transform.rescaleX(x);
      localPath.attr("d", line({ xKey: "time", yKey: "value", x: newX, y }));
    };

    if (zoomable) {
      zoomManager?.registerAction(zoomAction);
    }

    return localPath;
  };

  return path;
};
