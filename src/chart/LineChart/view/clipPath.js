export const getClipPath = (config) => {
  const { width, height, marginRight, marginLeft, marginTop, marginBottom } = config;
  const innerWidth = width - marginRight - marginLeft;
  const innerHeight = height - marginTop - marginBottom;

  const clipPath = (selection) =>
    selection
      .append("clipPath")
      .attr("id", "chart-view")
      .append("rect")
      .attr("width", innerWidth)
      .attr("height", innerHeight);

  return clipPath;
};
