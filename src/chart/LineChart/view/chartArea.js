import { getPath } from "./path";

export function getLineChart(config) {
  const { id, width, height, marginRight, marginLeft, marginTop, marginBottom, backgroundColor, zoom, maxZoom } =
    config;
  const innerWidth = width - marginRight - marginLeft;
  const innerHeight = height - marginTop - marginBottom;

  const path = getPath({ strokeWidth: 2, ...config });

  const background = (selection) =>
    selection.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("fill", backgroundColor);

  const chartArea = (selection) => {
    selection
      .append("g")
      .attr("id", `${id}-chart`)
      .attr("clip-path", "url(#chart-view)")
      .attr("transform", `translate(${marginLeft}, ${marginTop})`)
      .call(background)
      .call(path)
      .call(zoom)
      .transition()
      .duration(1000)
      .call(zoom.scaleTo, maxZoom / 2, [0, innerHeight / 2]);
  };

  return chartArea;
}
