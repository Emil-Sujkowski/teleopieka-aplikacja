import * as d3 from "d3";

export const getThresholds = (config) => {
  const {
    width,
    marginRight,
    marginLeft,
    thresholdMax,
    thresholdMin,
    scales: { y },
  } = config;

  const thresholdTop = (selection) => {
    const pathData = d3.path();
    pathData.moveTo(marginLeft, y(thresholdMax));
    pathData.lineTo(width - marginRight, y(thresholdMax));

    return selection
      .append("path")
      .attr("stroke", "#ff4040")
      .attr("stroke-width", 2)
      .attr("fill", "none")
      .style("stroke-dasharray", "3, 3")
      .attr("d", pathData);
  };

  const thresholdBottom = (selection) => {
    const pathData = d3.path();
    pathData.moveTo(marginLeft, y(thresholdMin));
    pathData.lineTo(width - marginRight, y(thresholdMin));

    return selection
      .append("path")
      .attr("stroke", "#4040ff")
      .attr("stroke-width", 2)
      .attr("fill", "none")
      .style("stroke-dasharray", "3, 3")
      .attr("d", pathData);
  };

  return { thresholdTop, thresholdBottom };
};
