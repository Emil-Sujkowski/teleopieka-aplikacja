import * as d3 from "d3";

import { getZoomManager } from "../../utils/zoom";

export const getAxes = (config) => {
  const {
    id,
    height,
    marginTop,
    marginBottom,
    marginLeft,
    scales: { x, y },
  } = config;

  const zoomManager = getZoomManager(id);

  const xAxis = (selection) => {
    const localAxis = selection
      .append("g")
      .attr("transform", `translate(${marginLeft}, ${height + marginTop - marginBottom})`)
      .call(d3.axisBottom(x).tickFormat((d) => `${d}s`));

    const zoomAction = (transform) => {
      const scaleX = transform.rescaleX(x);

      localAxis.call(d3.axisBottom(scaleX).tickFormat((d) => `${d}s`));
    };

    zoomManager?.registerAction(zoomAction);

    return localAxis;
  };

  const yAxis = (selection) =>
    selection.append("g").attr("transform", `translate(${marginLeft}, ${marginTop})`).call(d3.axisLeft(y));

  return { xAxis, yAxis };
};
