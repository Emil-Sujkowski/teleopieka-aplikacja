import * as d3 from "d3";
import { getZoomManager } from "../../utils/zoom";
import { getPath } from "./path";

export const getMnimap = (config) => {
  const { id, backgroundColor, zoom, minimap: minimapModel } = config;
  if (!minimapModel) return (selection) => {};

  const { x, y, width, height, marginLeft, marginRight, marginTop, marginBottom } = minimapModel;
  const pathEvents = { zoomable: false, pointerEvents: "none" };
  const windowID = `${id}-minimap-window`;
  const innerWidth = width - marginLeft - marginRight;
  const innerHeight = height - marginTop - marginBottom;
  const zoomManager = getZoomManager(id);
  const extent = [
    [0, 0],
    [innerWidth, innerHeight],
  ];

  const path = getPath(minimapModel, pathEvents);

  const background = (selection) =>
    selection
      .append("rect")
      .attr("width", innerWidth)
      .attr("height", innerHeight)
      .attr("fill", backgroundColor)
      .attr("pointer-events", "auto");

  const viewWindow = (selection) => {
    const localViewWindow = selection
      .append("rect")
      .attr("id", windowID)
      .attr("width", innerWidth)
      .attr("height", innerHeight)
      .attr("fill", "#000000")
      .attr("opacity", 0)
      .attr("pointer-events", "auto");

    const zoomAction = (transform) => {
      const scaledWidth = innerWidth / transform.k;
      d3.select(`#${id}-minimap`).node().__zoom.x = -transform.x / transform.k;
      localViewWindow.attr("width", scaledWidth).attr("transform", `translate(${-transform.x / transform.k}, 0)`);
    };

    zoomManager.registerAction(zoomAction);

    return localViewWindow;
  };

  const minimap = (selection) => {
    const localMinimap = selection
      .append("g")
      .attr("id", `${id}-minimap`)
      .attr("transform", `translate(${x + marginLeft}, ${y + marginTop})`)
      .on("mouseover", () => localMinimap.select(`#${windowID}`).attr("opacity", 0.2))
      .on("mouseout", () => localMinimap.select(`#${windowID}`).attr("opacity", 0));

    const panning = d3
      .zoom()
      .extent(extent)
      .on("start", ({ transform }) => {})
      .on("zoom", ({ transform }) => {
        const chart = d3.select(`#${id}-chart`);
        const ratio = chart.node().__zoom.k;
        const scaledWidth = innerWidth / ratio;
        localMinimap.select(`#${windowID}`).attr("transform", `translate(${transform.x}, 0)`);
        chart.call(zoom.translateTo, transform.x + scaledWidth / 2, transform.y);
      })
      .on("end", ({ transform }) => {});

    localMinimap.call(background);
    localMinimap.call(path);
    localMinimap.call(viewWindow);
    localMinimap.call(panning).on("wheel.zoom", null).on("dblclick.zoom", null);
  };

  return minimap;
};
