export const chartConfig = {
  marginLeft: 40,
  marginRight: 10,
  marginTop: 10,
  marginBottom: 40,
  minimapConfig: {
    marginLeft: 40,
    marginRight: 10,
    marginTop: 0,
    marginBottom: 0,
    height: 50,
  },
};
