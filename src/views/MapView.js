import { Marker, Popup } from "react-leaflet";
import { Navbar } from "../components/Navbar";
import { MapContainer } from 'react-leaflet/MapContainer'
import { TileLayer } from 'react-leaflet/TileLayer'
import { Icon } from "leaflet";
import "leaflet/dist/leaflet.css";
import { useEffect, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import PatientService from "../services/PatientService";

export default function MapView() {

    const mapRef = useRef();
    const [patients, setPatients] = useState([]);
    const navigate = useNavigate();

    const startPosition = [53.014, 18.598]; // Toruń
    const zoom = 11;

    const attribution = "&copy; Teleopieka";

    const marker = new Icon({
        iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
        iconSize: [24, 42],
        iconAnchor: [10, 42],
        popupAnchor: [2, -40]
    });

    useEffect(() => {
        PatientService.getPatients()
            .then((response) => {
                setPatients(Object.entries(response.data));
            })
            .catch((error) => {
                alert(error.response.data.detail);
            });
    }, [navigate]);

    return (
        <>
            <Navbar />
            <MapContainer
                ref={mapRef}
                center={startPosition}
                zoom={zoom}
                scrollWheelZoom={true}
            >
                <TileLayer
                    attribution={attribution}
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />

                {patients.map(([id, patient]) => {
                    const position = [patient.location.latitude, patient.location.longitude];
                    return (
                        <Marker
                            key={position[0]}
                            icon={marker}
                            position={[position[0], position[1]]}
                        >
                            <Popup>
                                <Link to={`/patient/${id}`}>
                                    {`${patient.firstname} ${patient.lastname}`}
                                </Link>
                            </Popup>
                        </Marker>
                    );
                })}
            </MapContainer>
        </>
    );

}