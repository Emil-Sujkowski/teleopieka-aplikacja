import { useEffect, useState } from "react";
import { MainContent } from "../components/MainContent";
import { Navbar } from "../components/Navbar";
import BandService from "../services/BandService";
import PatientService from "../services/PatientService";
import { Icon } from "../components/Icon";
import { OutlineButton } from "../components/Buttons";

export default function RequestsView() {

    const [requests, setRequests] = useState([]);
    const [patientsMap, setPatientsMap] = useState({});

    const loadRequests = () => {

        BandService.getBandRequests()
            .then((response) => {
                setRequests(response.data);
                let map = {};
                response.data.forEach(req => {
                    PatientService.getPatient(req.patient_id)
                        .then((res) => {
                            map[req.patient_id] = {
                                name: `${res.data.firstname} ${res.data.lastname}`
                            };
                            setPatientsMap(map);
                        });
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const acceptRequest = (req_id) => {

        BandService.acceptBand(req_id)
            .then(() => {
                alert('Opaska zaakceptowana');
                loadRequests();
            })
            .catch(error => console.log(error));
    }

    const rejectRequest = (req_id) => {

        BandService.rejectBand(req_id)
            .then(() => {
                alert('Opaska odrzucona');
                loadRequests();
            })
            .catch(error => console.log(error));
    }

    useEffect(() => {
        loadRequests();
    }, []);

    return (
        <>
            <Navbar />
            <MainContent title="Żądania zmiany opasek">
                <div className="overflow-x-auto m-2">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Pacjent</th>
                                <th>ID żądania</th>
                                <th>ID opaski</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {requests.map((request) => {
                                return (
                                    <>
                                        <tr key={request.request_id} className="hover text-base">
                                            <td>
                                                <div className="flex flex-row items-center">
                                                    <Icon name="user" />
                                                    <div className="flex flex-col items-start ml-3">
                                                        <div className="font-bold">
                                                            {patientsMap[request.patient_id] && patientsMap[request.patient_id].name}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {request.request_id}
                                            </td>
                                            <td>
                                                {request.band_id}
                                            </td>
                                            <td>
                                                <OutlineButton onClick={() => acceptRequest(request.request_id)} type="btn-success">
                                                    Akceptuj
                                                </OutlineButton>
                                            </td>
                                            <td>
                                                <OutlineButton onClick={() => rejectRequest(request.request_id)} type="btn-error">
                                                    Odrzuć
                                                </OutlineButton>
                                            </td>
                                        </tr>
                                    </>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </MainContent>
        </>
    );

}