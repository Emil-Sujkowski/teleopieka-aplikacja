import { useEffect, useState } from "react";
import { MainContent } from "../components/MainContent";
import { Navbar } from "../components/Navbar";
import { PatientSearchForm } from "../components/PatientSearchForm";
import AdminService from "../services/AdminService";
import { useNavigate } from "react-router-dom";
import { OutlineButton } from "../components/Buttons";
import { Badge } from "../components/Badge";
import { Modal } from "../components/Modal";
import { SimpleForm } from "../components/SimpleForm";
import { Icon } from "../components/Icon";

export default function AdminPanel() {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);

  // Wyszukiwanie użytkowników
  const [isSearching, setIsSearching] = useState(false);
  const [matchingUsers, setMatchingUsers] = useState([]);

  // Edycja użytkowników
  const [editedUserId, setEditedUserId] = useState("");
  const [editedFirstName, setEditedFirstName] = useState("");
  const [editedLastName, setEditedLastName] = useState("");
  const [editedPassword, setEditedPassword] = useState("");

  // Role
  const roles = {
    admin: {
      type: "badge-error",
      label: "Admin",
    },
    doctor: {
      type: "badge-primary",
      label: "Lekarz",
    },
  };

  const searchUsers = (patient) => {
    const pattern = {
      firstname_pattern: patient.firstName,
      lastname_pattern: patient.lastName,
    };

    AdminService.searchUsers(pattern)
      .then((response) => {
        setIsSearching(true);
        setMatchingUsers(Object.entries(response.data));
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  const dateOptions = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };

  const editFormInputs = [
    {
      text: "Imię",
      type: "text",
      placeholder: "Imię",
      value: editedFirstName,
      onChange: (e) => setEditedFirstName(e.target.value),
    },
    {
      text: "Nazwisko",
      type: "text",
      placeholder: "Nazwisko",
      value: editedLastName,
      onChange: (e) => setEditedLastName(e.target.value),
    },
    {
      text: "Hasło",
      type: "text",
      placeholder: "Hasło",
      value: editedPassword,
      onChange: (e) => setEditedPassword(e.target.value),
    },
  ];

  const loadUsers = () => {
    AdminService.getUsers()
      .then((response) => {
        setUsers(Object.entries(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    loadUsers();
  }, [navigate]);

  const getBadge = (id, role) => {
    const { type, label } = roles[role] || { type: "badge-secondary", label: "Użytkownik" };

    return <Badge key={role} allowDelete type={type} label={label} handleDelete={() => onBadgeDelete(id, role)} />;
  };

  const onBadgeDelete = (id, role) => {
    AdminService.deleteRole(id, role)
      .then(() => {
        loadUsers();
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  const onBadgeAdd = (id, role, dropdown) => {
    AdminService.addRole(id, role)
      .then(() => {
        dropdown.removeAttribute("open"); // zamknij dropdown
        loadUsers();
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  const selectUserToChange = (modalName, user, id) => {
    setEditedUserId(id);
    setEditedFirstName(user.firstname);
    setEditedLastName(user.lastname);
    setEditedPassword("");

    document.getElementById(modalName).showModal();
  };

  const onUserEdit = () => {
    // przesłanie zmienionych danych do API

    let data = {
      firstname: editedFirstName,
      lastname: editedLastName,
    };

    if (editedPassword !== "") {
      data.password = editedPassword;
    }

    AdminService.editUser(editedUserId, data)
      .then(() => {
        document.getElementById("edit_user").close();
        loadUsers();
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  const onUserDelete = () => {
    // usunięcie użytkownika przez API
    AdminService.deleteUser(editedUserId)
      .then(() => {
        document.getElementById("delete_user").close();
        document.getElementById("user_deleted").showModal();
        loadUsers();
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  return (
    <>
      <Navbar />

      <MainContent title="Wyszukaj użytkownika">
        <PatientSearchForm handleSubmit={searchUsers} />
      </MainContent>

      <MainContent title="Lista użytkowników">
        <div className="overflow-x-auto m-2">
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Imię i Nazwisko</th>
                <th>Role</th>
                <th>Ostatnie logowanie</th>
                <th>Edytuj</th>
                <th>Usuń</th>
              </tr>
            </thead>
            <tbody>
              {(isSearching ? matchingUsers : users).map(([id, user]) => {
                return (
                  <>
                    <tr key={id} className="hover text-base">
                      <td></td>
                      <td>
                        <div className="flex flex-row items-center">
                          <Icon name="user" />
                          <div className="flex flex-col items-start ml-3">
                            <div className="font-bold">
                              {user.firstname} {user.lastname}
                            </div>
                            <span className="text-sm">{user.email}</span>
                          </div>
                        </div>
                      </td>
                      <td>
                        {user.user_roles.map((role) => {
                          return getBadge(id, role);
                        })}

                        {
                          // nie wyświetlaj (+) gdy nie ma dostępnych ról
                          Object.keys(roles).length !== user.user_roles.length && (
                            <details className="dropdown">
                              <summary className="btn btn-success btn-sm rounded-full text-white">+</summary>

                              <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-52">
                                {Object.keys(roles).map((role) => {
                                  const { label } = roles[role];

                                  if (!user.user_roles.includes(role)) {
                                    return (
                                      <li>
                                        <a onClick={(event) => onBadgeAdd(id, role, event.target.closest("details"))}>
                                          {label}
                                        </a>
                                      </li>
                                    );
                                  }
                                })}
                              </ul>
                            </details>
                          )
                        }
                      </td>
                      <td>{new Date(user.last_login_date).toLocaleDateString("pl-PL", dateOptions)}</td>
                      <td>
                        <OutlineButton onClick={() => selectUserToChange("edit_user", user, id)}>Edytuj</OutlineButton>
                      </td>
                      <td>
                        <OutlineButton onClick={() => selectUserToChange("delete_user", user, id)} type="btn-error">
                          Usuń
                        </OutlineButton>
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
      </MainContent>

      <Modal id="edit_user" title="Edytuj użytkownika">
        <SimpleForm items={editFormInputs} submitText="Zapisz zmiany" handleSubmit={onUserEdit} />
      </Modal>

      <Modal id="delete_user" title="Alert">
        <p className="mt-4">
          Czy na pewno chcesz usunąć użytkownika{" "}
          <b>
            {editedFirstName} {editedLastName}{" "}
          </b>{" "}
          ({editedUserId})?{" "}
        </p>
        <div className="flex flex-row justify-evenly items-center mx-16 mt-4">
          <OutlineButton onClick={() => document.getElementById("delete_user").close()} type="btn-success">
            Anuluj
          </OutlineButton>
          <OutlineButton onClick={() => onUserDelete()} type="btn-error">
            Usuń
          </OutlineButton>
        </div>
      </Modal>

      <Modal id="user_deleted" title="Alert">
        <p className="mt-4">
          Użytkownik{" "}
          <b>
            {editedFirstName} {editedLastName}{" "}
          </b>{" "}
          ({editedUserId}) został usunięty z bazy.{" "}
        </p>
      </Modal>
    </>
  );
}
