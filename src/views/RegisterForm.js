import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { MainContent } from "../components/MainContent";
import { SimpleForm } from "../components/SimpleForm";
import UserService from "../services/UserService";
import { Navbar } from "../components/Navbar";

export default function RegisterForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const navigate = useNavigate();

  const handleRegister = () => {
    if (password !== repeatPassword) {
      alert("Hasła nie są takie same");
      return;
    }

    const credentials = {
      firstname: firstName,
      lastname: lastName,
      email: email,
      password: password,
    }

    UserService.register(credentials)
      .then((response) => {
        // jeśli sukces to przekieruj do logowania
        navigate("/");
      })
      .catch((error) => {
        // jeśli błąd to wyświetl
        if (error.response) {
          alert(`Podczas rejestracji wystąpił błąd:\n ${error.response.data.detail}`);
        }
      });
  };

  const formInputs = [
    {
      text: "Imię",
      type: "text",
      placeholder: "Imię",
      value: firstName,
      onChange: (e) => setFirstName(e.target.value),
    },
    {
      text: "Nazwisko",
      type: "text",
      placeholder: "Nazwisko",
      value: lastName,
      onChange: (e) => setLastName(e.target.value),
    },
    {
      text: "E-mail",
      type: "email",
      placeholder: "E-mail",
      value: email,
      onChange: (e) => setEmail(e.target.value),
    },
    {
      text: "Hasło",
      type: "password",
      placeholder: "Hasło",
      value: password,
      onChange: (e) => setPassword(e.target.value),
    },
    {
      text: "Powtórz hasło",
      type: "password",
      placeholder: "Powtórz hasło",
      value: repeatPassword,
      onChange: (e) => setRepeatPassword(e.target.value),
    },
  ];

  return (
    <>
      <Navbar hideItems={true} />
      <MainContent size='small' title="Rejestracja">
        <SimpleForm items={formInputs} submitText="Zarejestruj się" handleSubmit={handleRegister} />
        <Link to={`/`} className="text-secondary container flex justify-center">
          Zaloguj się
        </Link>
      </MainContent>
    </>

  );
}
