import "../Base.css";

import PatientLineChart from "../components/patientLineChart";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { MainContent } from "../components/MainContent";
import PatientService from "../services/PatientService";
import { DataText } from "../components/DataText";
import { Icon } from "../components/Icon";
import { Navbar } from "../components/Navbar";
import BandService from "../services/BandService";
import { SimpleForm } from "../components/SimpleForm";
import UploadForm from "../components/UploadForm";
import { DiseasesSelector } from "../components/DiseasesSelector";
import { Modal } from "../components/Modal";
import { OutlineButton } from "../components/Buttons";
import { ExtendedPatientChart } from "../components/ExtendedPatientChart";

export default function PatientProfile() {
  const { id } = useParams();

  const navigate = useNavigate();
  const [patient, setPatient] = useState({});
  const [bandId, setBandId] = useState();
  const [bandExists, setBandExists] = useState(true);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    PatientService.getPatient(id)
      .then((response) => {
        setPatient(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response.status === 403) {
          navigate("/"); // użytkownik nie jest zalogowany
        }
      });

    BandService.getBandId(id)
      .then((response) => {
        setBandId(response.data.band_id);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            navigate("/"); // użytkownik nie jest zalogowany
          } else if (error.response.status === 404) {
            setBandId("");
            setBandExists(false);
          }
        }
      });
  }, [navigate, id]);

  const formInputs = [
    {
      text: "ID Opaski",
      type: "text",
      placeholder: "ID Opaski",
      value: bandId,
      onChange: (e) => setBandId(e.target.value),
    },
  ];

  const updateBandState = () => {
    BandService.addBandId(id, bandId)
      .then(() => {
        alert("Wysłano żądanie zmiany ID opaski");
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  const arhythmiaMap = new Map([
    ["bradyarrhythmia", "Bradyarytmia"],
    ["tachycardi", "Tachykardia"]
  ]);

  return (
    <>
      <Navbar />
      <MainContent size="medium" title="Karta pacjenta">
        <div className="grid grid-cols-2 gap-4 mt-4 relative">
          <DataText header="Imię i nazwisko" text={`${patient.firstname} ${patient.lastname}`}></DataText>
          <DataText header="Płeć" text={patient.Gender}></DataText>
          <DataText header="PESEL" text={patient.PESEL}></DataText>
          <DataText header="Wiek" text={patient.Age}></DataText>
          {
            patient.color === "RED" &&
            <div className="absolute top-4 right-12">
              <div className="flex flex-col justify-center items-center">
                <Icon name="warning" size={64} />
                <span className="label-text font-bold text-error">
                  {arhythmiaMap.get(patient.arrhythmia[0])}
                </span>
              </div>
            </div>
          }

        </div>

        <div className="grid grid-cols-1 gap-4 mt-4">
          <DataText header="Adres" text={patient.address}></DataText>
          <DataText
            header="Lekarz prowadzący"
            text={`${patient.doctor_firstname} ${patient.doctor_lastname}`}
          ></DataText>
        </div>

        <div className="flex flex-row justify-center items-center my-4">
          {Object.keys(patient).length !== 0 && (
            <PatientLineChart
              id="patient-line-chart"
              width={600}
              height={300}
              minimap={true}
              backgroundColor={"#faf7f5"}
              color={"#e74f2a"}
              data={patient ? patient : null}
              dataKey={"value"}
            />
          )}
        </div>

        {/* <OutlineButton onClick={() => document.getElementById("ekg-popup").showModal()}>Pełny obraz</OutlineButton>
        <Modal id="ekg-popup" title="Badanie EKG">
          <ExtendedPatientChart data={patient["ecg"]}></ExtendedPatientChart>
        </Modal> */}

        <div className="container flex justify-center">
          <UploadForm label="Prześlij dane" acceptTypes={".csv"} />
        </div>

        <hr className="separator" />
        <h3 className="text-2xl mt-4 mb-8 text-center">Lista chorób pacjenta</h3>

        {!loading && <DiseasesSelector patientId={id} patientDiseases={patient.diseases} />}

        <hr className="separator" />
        <h3 className="text-2xl mt-4 mb-8 text-center">Opaska</h3>

        <SimpleForm items={formInputs} submitText="Zmień" handleSubmit={updateBandState} />
      </MainContent>
    </>
  );
}
