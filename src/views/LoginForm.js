import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { MainContent } from "../components/MainContent";
import { SimpleForm } from "../components/SimpleForm";
import TokenService from "../services/TokenService";
import UserService from "../services/UserService";
import { Navbar } from "../components/Navbar";

export default function LoginForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleLogin = () => {

    UserService.login(username, password)
      .then((response) => {
        TokenService.setAccessToken(response.data.access_token);
        TokenService.setRefreshToken(response.data.refresh_token);
      })
      .then(() => {
        UserService.getMe()
            .then((response) => {
                if (response.data.user_roles.includes('doctor')) {
                  navigate("/patients");
                } else if (response.data.user_roles.includes('admin')) {
                  navigate("/admin");
                }
            })
            .catch((error) => {
                alert(error.response.data.detail);
            });
      })
      .catch((error) => {
        if (error.response.status === 401) {
          alert("Błędne dane logowania");
        } else {
          alert(`Podczas logowania wystąpił błąd:\n ${error.message}`);
        }
      });
  };

  const formInputs = [
    {
      text: "E-mail",
      type: "email",
      placeholder: "E-mail",
      value: username,
      onChange: (e) => setUsername(e.target.value),
    },
    {
      text: "Hasło",
      type: "password",
      placeholder: "Hasło",
      value: password,
      onChange: (e) => setPassword(e.target.value),
    },
  ];

  return (
    <>
      <Navbar hideItems={true} />
      <MainContent size='small' title="Logowanie">
        <SimpleForm items={formInputs} submitText="Zaloguj się" handleSubmit={handleLogin} />
        <Link to={`/register`} className="text-secondary container flex justify-center">
          Zarejestruj się
        </Link>
      </MainContent>
    </>

  );
}
