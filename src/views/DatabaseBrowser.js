import { useNavigate } from "react-router-dom";
import "../Base.css";
import { useEffect, useState } from "react";
import { MainContent } from "../components/MainContent";
import PatientService from "../services/PatientService";
import { PatientSearchForm } from "../components/PatientSearchForm";
import { Navbar } from "../components/Navbar";
import { PatientDataRow } from "../components/PatientDataRow";

const DATA_TYPE = Object.freeze({
  ecg: "ecg",
  ppg: "ppg",
});

export default function DatabaseBrowser() {
  const navigate = useNavigate();
  const [patients, setPatients] = useState([]);
  const [chartData, setChartData] = useState(DATA_TYPE.ecg);

  // Wyszukiwanie pacjentów
  const [isSearching, setIsSearching] = useState(false);
  const [matchingUsers, setMatchingUsers] = useState([]);

  useEffect(() => {
    PatientService.getPatients()
      .then((response) => {
        setPatients(Object.entries(response.data));
      })
      .catch((error) => {
        alert(error.response.data.detail);
      });
  }, [navigate]);

  const searchPatients = (patient) => {
    console.log(patient)
    const pattern = {
      firstname_pattern: patient.firstName,
      lastname_pattern: patient.lastName,
      PESEL_pattern: patient.pesel
    };

    PatientService.searchPatients(pattern)
      .then((response) => {
        setIsSearching(true);
        setMatchingUsers(Object.entries(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <Navbar />

      <MainContent title="Wyszukaj pacjenta">
        <PatientSearchForm handleSubmit={searchPatients} />
      </MainContent>

      <MainContent title="Lista pacjentów">
        <div className="overflow-x-auto m-2">
          <table className="table">
            <thead>
              <tr>
                <th>Imię i Nazwisko</th>
                <th>PESEL</th>
                <th>Płeć</th>
                <th>Wiek</th>
                <th>Przypisany lekarz</th>
                <th>Stan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {(isSearching ? matchingUsers : patients).map(([id, patient]) => (
                <PatientDataRow id={id} patient={patient}></PatientDataRow>
              ))}
            </tbody>
          </table>
        </div>
      </MainContent>
    </>
  );
}
