import { useEffect, useState } from "react";
import { Navbar } from "../components/Navbar";
import { MainContent } from "../components/MainContent";
import { SimpleForm } from "../components/SimpleForm";
import Selector from "../components/Selector";
import UserService from "../services/UserService";
import PatientService from "../services/PatientService";
import { useNavigate } from "react-router-dom";

export default function DoctorPanel() {
    const navigate = useNavigate();
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [pesel, setPesel] = useState("");
    const [birthday, setBirthday] = useState(new Date());
    const [gender, setGender] = useState("");
    const [address, setAddress] = useState("");
    const [docId, setDocId] = useState("");

    useEffect(() => {
        UserService.getMe()
            .then((response) => {
                if (response.data.user_roles.includes('doctor')) {
                    setDocId(response.data.id);
                }
            })
            .catch((error) => {
                alert(error.response.data.detail);
            });
    }, [navigate]);

    const formInputs = [
        {
            text: "Imię",
            type: "text",
            placeholder: "Imię",
            value: firstname,
            onChange: (e) => setFirstname(e.target.value),
        },
        {
            text: "Nazwisko",
            type: "text",
            placeholder: "Nazwisko",
            value: lastname,
            onChange: (e) => setLastname(e.target.value),
        },
        {
            text: "PESEL",
            type: "text",
            placeholder: "PESEL",
            value: pesel,
            onChange: (e) => setPesel(e.target.value),
        },
        {
            text: "Data urodzenia",
            type: "date",
            placeholder: "Data urodzenia",
            value: birthday,
            onChange: (e) => setBirthday(e.target.value),
        },
        {
            text: "Adres",
            type: "text",
            placeholder: "Adres",
            value: address,
            onChange: (e) => setAddress(e.target.value),
        },

    ];

    const reset = () => {
        setFirstname("");
        setLastname("");
        setPesel("");
        setBirthday(new Date());
        setGender("");
        setAddress("");
    }

    const handleSubmit = () => {
        const data = {
            "firstname": firstname,
            "lastname": lastname,
            "PESEL": pesel,
            "birthday": new Date(birthday).toISOString(),
            "Gender": gender,
            "address": address,
            "doctor_id": docId
        }

        PatientService.addPatient(data)
            .then(() => {
                alert(`Dodano pacjenta ${firstname} ${lastname}`);
                reset();
            })
            .catch((error) => {
                if (error.response) {
                    alert(error.response.data.detail);
                }
            });
    }

    const genderOptions = [
        { value: 'M', label: 'Mężczyzna' },
        { value: 'F', label: 'Kobieta' },
    ];

    return (
        <>
            <Navbar />
            <MainContent size='small' title="Dodaj pacjenta">
                <SimpleForm items={formInputs} submitText="Dodaj" handleSubmit={handleSubmit}>
                    <Selector label="Płeć" options={genderOptions} handleChange={(g) => setGender(g)} />
                </SimpleForm>
            </MainContent>
        </>

    );
}