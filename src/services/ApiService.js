import axios from "axios";
import Config from "../Config";
import TokenService from "./TokenService";

const instance = axios.create({
    baseURL: Config.apiUrl,
    headers: {
        accept: "application/json"
    },
});

instance.interceptors.request.use(
    config => {
        const token = config.url === '/user/refresh_token'
            ? TokenService.getRefreshToken() : TokenService.getAccessToken();
        // do odswieżenia użyj refresh tokena

        if (token) {
            config.headers["Authorization"] = 'Bearer ' + token;
        }

        return config;
    },

    error => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(

    res => {
        return res;
    },

    async error => {
        const config = error.config;

        if (config.url !== '/user/login' && error.response) {
            if (error.response.status === 401 && !config._retry) {
                // token wymaga odświeżenia

                config._retry = true; // dla uniknięcia pętli

                try {
                    const res = await instance.post('/user/refresh_token', {}, config);
                    const { access_token } = res.data;
                    TokenService.updateAccessToken(access_token);

                    return instance(config);
                } catch (error) {
                    if (error.response.status === 401) { // użytkownik nie jest zalogowany
                        window.location.href = "/";
                    }
                    return Promise.reject(error);
                }
            }
        }

        return Promise.reject(error);
    }
);

export default instance;