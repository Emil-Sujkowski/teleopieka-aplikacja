import api from '../services/ApiService';

class BandService {

    getBandId(patientId) {
        return api.get(`/band/get_band_id/${patientId}`);
    }

    addBandId(patientId, bandId) {
        return api.post('/band/change_band_id_request', {
            "patient_id": patientId,
            "band_id": bandId
        });
    }

    getBandRequests() {
        return api.get('/band/get_requests');
    }

    acceptBand(requestId) {
        return api.post(`/band/accept_request?request_id=${requestId}`);
    }

    rejectBand(requestId) {
        return api.post(`/band/reject_request?request_id=${requestId}`);
    }
}

export default new BandService();