import api from '../services/ApiService';

class AdminService {

    getUsers() {
        return api.get('/superuser/users');
    }

    editUser(email, data) {
        return api.post('/superuser/edit_user_data', {
            email: email,
            user_data: data,
        });
    }

    deleteUser(email) {
        return api.delete('/superuser/user_delete', {
            params: {
                email: email
            }
        });
    }

    searchUsers(pattern) {
        return api.get('/superuser/users/pattern', {
            params: pattern
        });
    }

    addRole(email, role) {
        return api.post('/superuser/add_user_role', {
            email: email,
            user_role: role,
        });
    }

    deleteRole(email, role) {
        return api.delete('/superuser/remove_user_role', {
            data: {
                email: email,
                user_role: role
            }
        });
    }
}

export default new AdminService();