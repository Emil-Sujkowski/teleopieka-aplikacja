import api from '../services/ApiService';

class PatientService {

    getPatients() {
        return api.get('/patients');
    }

    getPatient(id) {
        return api.get(`/patients/id/${id}`);
    }

    searchPatients(pattern) {
        return api.get('/patients/pattern', {
            params: pattern
        });
    }

    updateDiseases(id, diseases) {
        return api.put(`/patients/diseases/edit?patient_id=${id}`,
            diseases);
    }

    addPatient(data) {
        return api.post('/patients/add', data);
    }
}

export default new PatientService();