class TokenService {
    getAccessToken() {
        const token = localStorage.getItem("access_token");
        return token;
    }

    getRefreshToken() {
        const token = localStorage.getItem("refresh_token");
        return token;
    }

    updateAccessToken(token) {
        localStorage.setItem("access_token", token);
    }

    setAccessToken(token) {
        localStorage.setItem("access_token", token);
    }

    setRefreshToken(token) {
        localStorage.setItem("refresh_token", token);
    }

    clearTokens() {
        localStorage.removeItem("access_token");
        localStorage.removeItem("refresh_token");
    }
}

export default new TokenService();