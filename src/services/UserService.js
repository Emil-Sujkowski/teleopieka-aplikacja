import api from '../services/ApiService';

class UserService {

    login(username, password) {
        return api.post('/user/login', {
            username: username,
            password: password,
        }, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
        });
    }

    register(credentials) {
        return api.post('/user/signup', credentials, {
            headers: {
                "Content-Type": "application/json",
            },
        });
    }

    getMe() {
        return api.get('/users/me');
    }
}

export default new UserService();