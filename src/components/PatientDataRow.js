import { Link } from "react-router-dom";
import { useMemo } from "react";
import PatientLineChart from "../components/patientLineChart";
import { Icon } from "./Icon";

export const PatientDataRow = (props) => {
  const { id, patient, chartData } = props;
  const userChart = useMemo(
    () => (
      <PatientLineChart
        key={id}
        id={`chart-${id}`}
        width={600}
        height={300}
        backgroundColor={"#faf7f5"}
        color={"#e74f2a"}
        data={patient ? patient : null}
        dataKey={"value"}
      />
    ),
    [id, patient]
  );

  const colorMap = new Map([
    ["GREEN", "Stabilny"],
    ["RED", "Wymaga uwagi"],
  ]);

  return (
    <>
      <tr key={id} className="hover text-base">
        <td>
          <div className="flex items-center gap-3">
            {
              patient.color === "RED" && <Icon name="warning" size={24} />
            }
            <Link to={`/patient/${id}`}>
              <div className="font-bold">
                {patient.firstname} {patient.lastname}
              </div>
            </Link>
          </div>
        </td>
        <td>{patient.PESEL}</td>
        <td>{patient.Gender}</td>
        <td>{patient.Age}</td>
        <td>{`${patient.doctor_firstname} ${patient.doctor_lastname}`}</td>
        <td className={patient.color === "RED" && "text-red-600 font-bold"}>
          {colorMap.get(patient.color)}
        </td> 
      </tr>
      <div className="flex flex-row justify-center items-center my-4">{patient.Ecg && userChart}</div>
    </>
  );
};
