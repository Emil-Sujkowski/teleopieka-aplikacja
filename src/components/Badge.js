export const Badge = (props) => {
    const { type, label, handleClick, handleDelete } = props;

    return (
        <div className={`badge ${type} p-4 gap-2 mr-2 ${handleClick ? "cursor-pointer" : ""}`} onClick={handleClick}>
            <span className="text-white font-bold">
                {label}
            </span>
            {
                handleDelete &&
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                    className="inline-block w-4 h-4 stroke-current text-white cursor-pointer"
                    onClick={handleDelete}>
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="4" d="M6 18L18 6M6 6l12 12">
                    </path>
                </svg>
            }

        </div>
    );
};
