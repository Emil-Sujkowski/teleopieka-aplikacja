import { useState } from "react";
import { OutlineButton } from "./Buttons";
import { FormInput } from "./FormInput";

const defaultSearchModel = Object.freeze({
  firstName: "",
  lastName: "",
  pesel: "",
  assignedDoc: "",
});

export const PatientSearchForm = (props) => {
  const { handleSubmit } = props;
  const [searchModel, setSearchModel] = useState(defaultSearchModel);

  const clearSearch = () => {
    setSearchModel(defaultSearchModel);
    handleSubmit(defaultSearchModel);
  };

  const items = [
    {
      text: "Imię",
      type: "text",
      placeholder: "Imię",
      value: searchModel.firstName,
      onChange: (e) => setSearchModel({ ...searchModel, firstName: e.target.value }),
    },
    {
      text: "Nazwisko",
      type: "text",
      placeholder: "Nazwisko",
      value: searchModel.lastName,
      onChange: (e) => setSearchModel({ ...searchModel, lastName: e.target.value }),
    },
    {
      text: "PESEL",
      type: "text",
      placeholder: "PESEL",
      value: searchModel.pesel,
      onChange: (e) => setSearchModel({ ...searchModel, pesel: e.target.value }),
    },
    {
      text: "Przypisany lekarz",
      type: "text",
      placeholder: "Przypisany lekarz",
      value: searchModel.assignedDoc,
      onChange: (e) => setSearchModel({ ...searchModel, assignedDoc: e.target.value }),
    },
  ];

  return (
    <div className="form-container flex flex-row items-center justify-center gap-8 mb-6">
      {items.map((input) => (
        <FormInput {...input}></FormInput>
      ))}

      <div className="flex flex-col gap-4 mt-6">
        <OutlineButton type="btn-error" onClick={clearSearch}>
          Wyczyść
        </OutlineButton>
        <OutlineButton type="btn-success" onClick={() => handleSubmit(searchModel)}>
          Zatwierdź
        </OutlineButton>
      </div>

      {props.children}
    </div>
  );
};
