import { useState } from "react";
import { OutlineButton } from "./Buttons";

export default function UploadForm({ label, handleSubmit, acceptTypes }) {
    const [selectedFile, setSelectedFile] = useState(null);

    const handleFileChange = (event) => {
        setSelectedFile(event.target.files[0]);
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label className="form-control w-full max-w-xs">
                    <div className="label">
                        <span className="label-text">{label}</span>
                    </div>
                    <input
                        className="file-input file-input-bordered file-input-primary w-full max-w-xs"
                        type="file"
                        name="fileUpload"
                        accept={acceptTypes}
                        onChange={handleFileChange}
                    />
                </label>
                <OutlineButton type="btn-success" disabled={!selectedFile}>Wyślij</OutlineButton>
            </form>
        </div>
    );
};
