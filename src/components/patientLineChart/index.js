import * as d3 from "d3";
import { useEffect, useRef, useState } from "react";
import { getModel } from "./model";
import { drawLineChart } from "../../chart/LineChart/view";
import { processData } from "./parseData";
import { DataJumpButton } from "./dataJumpButton";
import { DataTypeSelector } from "./dataTypeSelector";

export default function Chart(props) {
  const [dataType, setDataType] = useState("pulse");
  const data = processData(props.data);
  const chartRef = useRef(null);

  useEffect(() => {
    const { width, height } = props;
    const chartModel = getModel({ ...props, data, dataType });

    const svg = d3
      .select(`#${props.id}`)
      .append("svg")
      .data([data[dataType][dataType]])
      .attr("id", "chart")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height]);

    svg.call(drawLineChart, chartModel);

    return () => svg.remove();
  }, [props, data, dataType]);

  return (
    <div id={props.id} ref={chartRef}>
      <DataTypeSelector dataType={dataType} setDataType={setDataType}></DataTypeSelector>
      <DataJumpButton chartId={props.id} dataPoints={data[dataType].detectedPoints}></DataJumpButton>
    </div>
  );
}
