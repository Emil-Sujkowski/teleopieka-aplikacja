import { dataset } from "../../chart/MockData/mimicPerform";

export const processData = (data) => {
  const randomPoints = (arr, amount) => {
    return [...Array(amount)].map(() => ({ ...arr[Math.floor(Math.random() * arr.length)], text: "arrhythmia" }));
  };

  const pulse = data.Pulse?.Time.map((time, index) => ({
    time,
    value: data.Pulse?.Values?.[index],
  }));

  dataset.ecg = dataset.ecg.map((point) => ({ ...point, value: point[2] }));
  const ecg = dataset;

  const result = {
    ecg: {
      ...ecg,
      detectedPoints: randomPoints(ecg.ecg, 10).sort((a, b) => a.time - b.time),
    },
    pulse: {
      pulse,
      detectedPoints: randomPoints(pulse, 10).sort((a, b) => a.time - b.time),
    },
    thresholdMin: data.lowerThreshold,
    thresholdMax: data.upperThreshold,
  };

  return result;
};
