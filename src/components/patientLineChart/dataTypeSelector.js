export const DataTypeSelector = (props) => {
  const { dataType, setDataType } = props;

  return (
    <div className="dropdown">
      <div tabIndex={0} role="button" className="btn btn-accent m-1">
        {dataType}
      </div>
      <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
        <li>
          <a onClick={() => setDataType("ecg")}>ECG</a>
        </li>
        <li>
          <a onClick={() => setDataType("pulse")}>Pulse</a>
        </li>
      </ul>
    </div>
  );
};
