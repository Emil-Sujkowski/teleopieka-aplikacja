import { getZoomManager } from "../../chart/utils/zoom";

export const DataJumpButton = (props) => {
  const { chartId, dataPoints } = props;

  const itemClick = (point) => {
    const zoomManager = getZoomManager(chartId);
    let currentTransform = zoomManager.getCurrentTransform();
    currentTransform.x = 0;
    currentTransform.y = 0;
    currentTransform.x = currentTransform.applyX(-point.time);
    zoomManager.applyZoom({ transform: currentTransform });
  };

  return (
    <div className="dropdown">
      <div tabIndex={0} role="button" className="btn btn-accent m-1">
        Detected anomalies
      </div>
      <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
        {dataPoints.map((point) => (
          <li>
            <a onClick={() => itemClick(point)}>{`${point.text}: (${point.time})`}</a>
          </li>
        ))}
      </ul>
    </div>
  );
};
