import * as d3 from "d3";
import { chartConfig } from "../../../chart/LineChart/chartConfig";
import { getScales } from "../../../chart/utils/linearScales";
import { createZoomManager } from "../../../chart/utils/zoom";

export const getModel = (props) => {
  const { data, dataType, dataKey, minimap, id, width, height, backgroundColor, color } = props;
  const { thresholdMax, thresholdMin } = data;
  const { minimapConfig } = chartConfig;
  const numericData = data[dataType][dataType];

  const { marginLeft, marginRight, marginTop, marginBottom } = chartConfig;
  const dataReader = { domain: "time", value: dataKey ?? 2 };
  const minimapHeight = minimap ? minimapConfig.height : 0;
  const minimapWidth = minimap ? width : 0;
  const chartHeight = height - minimapHeight;
  const chartWidth = width;
  const maxZoom = dataType === "ecg" ? Math.round(numericData.length / width) : 10;

  const { x: chartX, y: chartY } = getScales(numericData, dataReader, {
    ...chartConfig,
    width: chartWidth,
    height: chartHeight,
  });
  const { x: minimapX, y: minimapY } = getScales(numericData, dataReader, {
    ...minimapConfig,
    width: minimapWidth,
    height: minimapHeight,
  });

  const extent = [
    [0, 0],
    [width - marginLeft - marginRight, height - marginTop - marginBottom],
  ];
  const scaleExtent = [1, maxZoom];
  const translateExtent = [
    [0, 0],
    [width - marginLeft - marginRight, height - marginTop - marginBottom],
  ];

  const zoomManager = createZoomManager(id);
  const zoom = d3
    .zoom()
    .extent(extent)
    .scaleExtent(scaleExtent)
    .translateExtent(translateExtent)
    .on(`start.${id}`, (event) => {})
    .on(`zoom.${id}`, (event) => {
      zoomManager.applyZoom(event);
    })
    .on(`end.${id}`, (event) => {});

  return {
    id,
    // dimensions
    marginLeft,
    marginRight,
    marginTop,
    marginBottom,
    width: chartWidth,
    height: chartHeight,
    // style
    color,
    backgroundColor,
    //config
    scales: { x: chartX, y: chartY },
    zoom,
    maxZoom,
    thresholdMax,
    thresholdMin,
    minimap: minimap && {
      id,
      marginLeft,
      marginRight,
      marginTop: 0,
      marginBottom: 0,
      x: 0,
      y: chartHeight,
      width: minimapWidth,
      height: minimapHeight,
      color,
      backgroundColor,
      scales: { x: minimapX, y: minimapY },
    },
  };
};
