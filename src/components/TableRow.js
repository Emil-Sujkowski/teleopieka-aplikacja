export const TableRow = (props) => {
  const { header, text } = props;

  return (
    <tr className="hover">
      <td className="font-bold">{header}</td>
      <td>{text}</td>
    </tr>
  );
};
