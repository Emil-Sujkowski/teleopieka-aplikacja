import PatientLineChart from "../components/patientLineChart";

export const ExtendedPatientChart = (props) => {
  const config = [
    {
      color: "#cc9933",
      backgroundColor: "#faf7f5",
      width: 400,
      height: 150,
      dataKey: 1,
      data: null,
    },
    {
      color: "#33ff00",
      backgroundColor: "#faf7f5",
      width: 400,
      height: 150,
      dataKey: 2,
      data: null,
    },
    {
      color: "#33cccc",
      backgroundColor: "#faf7f5",
      width: 400,
      height: 150,
      dataKey: 3,
      data: null,
    },
    {
      color: "#cc66cc",
      backgroundColor: "#faf7f5",
      width: 400,
      height: 150,
      dataKey: 4,
      data: null,
    },
    {
      color: "#e74f2a",
      backgroundColor: "#faf7f5",
      dataKey: 5,
      width: 400,
      height: 200,
      minimap: true,
      data: null,
    },
  ];

  return (
    <div>
      {config.map((chart) => (
        <PatientLineChart {...chart} id="extended-patient-chart"></PatientLineChart>
      ))}
    </div>
  );
};
