export const Checkbox = (props) => {
    const { label, checked, handleChange } = props;

    return (
        <label className="label" style={{ justifyContent: 'flex-start' }}>
            <input type="checkbox" checked={checked} className="checkbox checkbox-primary [--chkfg:white]"
                    onChange={handleChange} />
            <span className="label-text ml-4">{label}</span>
        </label>
    );
};
