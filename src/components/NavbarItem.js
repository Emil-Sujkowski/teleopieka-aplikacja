import { Link } from "react-router-dom";

export const NavbarItem = (props) => {
    const { text, path } = props;

    return (
        <li className="text-primary text-lg font-bold">
            <Link to={path}>
                {text}
            </Link>
        </li>
    );
};