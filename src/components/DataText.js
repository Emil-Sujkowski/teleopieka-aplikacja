export const DataText = (props) => {
    const { header, text } = props;

    return (
        <div className="flex flex-col mr-16">
            <span className="label-text font-bold" style={{color: '#a1a1a1'}}>{header}</span>
            <span>{text}</span>
        </div>
    );
};
