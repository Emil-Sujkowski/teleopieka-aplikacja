export const FormInput = (props) => {
  const { text, type, placeholder, value, onChange } = props;

  return (
    <label className="form-control w-full max-w-xs">
      <span className="label label-text">{text}</span>
      <input
        type={type}
        placeholder={placeholder}
        className="input input-bordered w-full max-w-xs"
        value={value}
        onChange={onChange}
      />
    </label>
  );
};
