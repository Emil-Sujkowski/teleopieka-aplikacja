export const OutlineButton = (props) => {
  const { type } = props;

  return <button {...props} className={`btn ${type || "btn-primary"} btn-outline`}></button>;
};
