import { useState } from "react";

export default function Selector({ label, handleChange, options }) {
    const [selectedOption, setSelectedOption] = useState('');

    const onChange = (event) => {
        setSelectedOption(event.target.value);
        handleChange(event.target.value);
    };

    return (
        <>
            <select className="select w-full max-w-xs" value={selectedOption} onChange={onChange}>
                <option value="">{label}</option>
                {options.map((option) => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </select>
        </>
    );
}