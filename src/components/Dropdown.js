import { OutlineButton } from "./Buttons";

export const Dropdown = (props) => {
  const { elements } = props;

  return (
    <div className="dropdown">
      <OutlineButton tabIndex={0}>Typ Wykresu</OutlineButton>
      <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
        {elements.map((el) => (
          <li>
            <div onClick={el.onClick}>{el.text}</div>
          </li>
        ))}
      </ul>
    </div>
  );
};
