import { OutlineButton } from "./Buttons";
import { FormInput } from "./FormInput";

export const SimpleForm = (props) => {
  const { items, submitText, handleSubmit } = props;

  return (
    <div className="form-container flex flex-col items-center justify-center gap-8 mb-6">
      {items.map((input) => (
        <FormInput {...input}></FormInput>
      ))}
      {props.children}
      <OutlineButton type="btn-success" onClick={handleSubmit}>{submitText}</OutlineButton>
    </div>
  );
};
