import { NavbarItem } from "./NavbarItem";
import { OutlineButton } from "./Buttons";
import TokenService from "../services/TokenService";
import UserService from "../services/UserService";
import { useEffect, useState } from "react";

export const Navbar = (props) => {
    const { hideItems } = props;
    const hide = hideItems || false;

    const logout = () => {
        TokenService.clearTokens();
        window.location.href = "/";
    }

    const [userRoles, setUserRoles] = useState([])

    const docLinks = [
        { path: "/patients", text: "Lista pacjentów" },
        { path: "/doctor", text: "Panel lekarza" },
        { path: "/map", text: "Mapa pacjentów" }
    ];

    const adminLinks = [
        { path: "/admin", text: "Panel administratora" },
        { path: "/requests", text: "Opaski" }
    ];

    useEffect(() => {
        if (!hide) {
            UserService.getMe()
                .then((response) => {
                    setUserRoles(response.data.user_roles);
                })
                .catch((error) => {
                    alert(error.response.data.detail);
                });
        }

    }, []);

    return (
        <div {...props}
            className="navbar bg-neutral rounded-b-3xl px-8"
            style={{ borderBottom: '2px solid #00A7EE' }}>
            <div className="navbar-start">
                <a className="btn btn-ghost text-3xl text-secondary">TELEOPIEKA</a>
            </div>
            {
                !hide &&
                <>
                    <div className="navbar-center hidden lg:flex">
                        <ul className="menu menu-horizontal px-1">
                            {
                                userRoles.includes('admin') &&
                                adminLinks.map((link) => {
                                    return (
                                        <NavbarItem path={link.path} text={link.text} />
                                    )
                                })
                            }
                            {
                                userRoles.includes('doctor') &&
                                docLinks.map((link) => {
                                    return (
                                        <NavbarItem path={link.path} text={link.text} />
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <div className="navbar-end">
                        <OutlineButton type="btn-error" onClick={logout}>Wyloguj</OutlineButton>
                    </div>
                </>
            }

            {props.children}
        </div>
    );
};