export const Icon = (props) => {
    const { name, size } = props;

    return (
        <div>
            <img src={`/icons/${name}.svg`} alt={name}
                style={{height: size, width: size}} />
        </div>
    );
};
