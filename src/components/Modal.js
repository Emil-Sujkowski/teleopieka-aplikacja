export const Modal = (props) => {
    const { title } = props;

    return (
        <dialog {...props} className="modal">
            <div className="modal-box bg-white">
                <h2 className="text-2xl mb-4 ml-4">{title}</h2>
                <hr className="separator" />
                <form method="dialog">
                    {/* wyjście */}
                    <button className="btn btn-sm btn-circle btn-error text-white absolute right-2 top-2">✕</button>
                </form>
                {props.children}
            </div>
        </dialog>
    );
};

