export const MainContent = (props) => {
  const { size, title } = props;

  let sizeClass;
  
  if (size === 'large') {
    sizeClass = "max-w-4xl";
  } else if (size === 'medium') {
    sizeClass = "max-w-2xl";
  } else if (size === 'small') {
    sizeClass = "max-w-lg";
  } else {
    sizeClass = "max-w-screen-xl";
  }

  return (
    <div {...props} className={`container ${sizeClass} shadow-md`}>
      <h2 className="text-3xl mb-4 ml-4">{title}</h2>
      <hr className="separator" />

      <div className="px-4">
        {props.children}
      </div>
    </div>
  );
};
