import { useState } from "react";
import PatientService from "../services/PatientService";
import { Checkbox } from "./Checkbox";

export const DiseasesSelector = (params) => {
  const { patientId, patientDiseases } = params;
  const [activeDiseases, setActiveDiseases] = useState(patientDiseases);

  const diseases = [
    "Arytmia serca",
    "Niewydolność serca",
    "Choroba wieńcowa",
    "Wady wrodzone serca",
    "Kardiomiopatia",
    "Zapalenie mięśnia sercowego",
    "Migotanie przedsionków",
    "Zapalenie osierdzia",
    "Nadciśnienie tętnicze",
    "Zawał serca",
  ];

  const updateDiseases = (disease, remove) => {
    let currentDiseases = activeDiseases;
    const index = currentDiseases.findIndex((d) => d === disease);

    if (remove && index !== -1) {
      currentDiseases.splice(index, 1);
    } else {
      currentDiseases.push(disease);
    }

    PatientService.updateDiseases(patientId, currentDiseases)
      .then(() => {
        setActiveDiseases([...currentDiseases]);
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data.detail);
        }
      });
  };

  return (
    <div className="form-control grid grid-cols-2 mb-4">
      {diseases.map((disease) => {
        const isActive = activeDiseases.includes(disease);
        return <Checkbox checked={isActive} label={disease} handleChange={() => updateDiseases(disease, isActive)} />;
      })}
    </div>
  );
};
