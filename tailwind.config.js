/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        teleopieka: {
          "primary": "#00A7EE",
          "secondary": "#005A7F",
          "accent": "#f59e0b",
          "neutral": "#ffffff",
          "base-100": "#EEEEEE",
          "info": "#00A7EE",
          "success": "#44B678",
          "warning": "#FAF02D",
          "error": "#EB2D4C",
        },
      },
      "cupcake"
    ],
  },
};
